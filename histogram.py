name = input("Enter file name: ")
file = open(name, "r")
table = {}

# collecting data into words dictionary and their frequency
for words in file:
    words = words.upper()
    if len(words) != 0:
        words = words.split()
        for i in words:
            if i.isalpha():
                if i in table:
                    table[i] += 1
                else:
                    table[i] = 1

file.close()
# sorting the dictionary based on values
table = dict(sorted(table.items(),key=lambda x: x[1],reverse=True))

#get the total sum of values
total=sum(table.values())
# printing the dictionary items form sorted dictionary
for key, value in table.items():
    perc=int((value/total)*100) #find the pecentage
    print(key, str(perc)+'%')